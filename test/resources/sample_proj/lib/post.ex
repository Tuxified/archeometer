defmodule SampleProj.Post do
  @moduledoc false
  use Ecto.Schema

  schema "posts" do
    field(:content, :string)
    belongs_to(:user, SampleProj.User)
    has_many(:post_comments, SampleProj.PostComment)
    has_many(:comments, through: [:post_comments, :comment])
  end
end
