defmodule SampleProj do
  @moduledoc """
  Documentation for `SampleProj`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> SampleProj.hello()
      :world

  """
  def hello do
    # Trigger a Credo check
    IO.inspect("Oh no!")
    :world
  end

  defmodule Foo do
    def bar(x) do
      # Trigger another Credo check
      x && x
    end
  end
end
