defmodule SampleProj.User do
  @moduledoc false
  use Ecto.Schema

  schema "users" do
    field(:name, :string)
    field(:enabled, :boolean)
    has_many(:posts, SampleProj.Post)
    has_many(:comments, through: [:posts, :post_comments, :comment])

    embeds_one :profile, Profile do
      field(:online, :boolean)
      field(:dark_mode, :boolean)
    end
  end
end
