defmodule SampleProj.Group do
  @moduledoc false
  use Ecto.Schema

  schema "groups" do
    field(:name, :string)
    many_to_many(:users, SampleProj.User, join_through: :users_groups)
  end
end
