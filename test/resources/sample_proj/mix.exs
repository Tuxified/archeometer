defmodule SampleProj.MixProject do
  use Mix.Project

  def project do
    [
      app: :sample_proj,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      deps_path: "../../../deps",
      lockfile: "../../../mix.lock"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:archeometer, path: "../../.."},
      {:ecto, "~> 3.0"}
    ]
  end
end
