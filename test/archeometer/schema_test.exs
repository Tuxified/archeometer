defmodule Archeometer.SchemaTest do
  use ExUnit.Case
  alias Archeometer.Schema.Assoc

  defmodule ModuleA do
    use Archeometer.Schema
    alias Archeometer.SchemaTest.ModuleB

    defschema :module_a do
      field(:first_module, primary_key: true)
      belongs_to(ModuleB)
      has(ModuleB, as: :other_b, key: :recurse_id)
    end
  end

  defmodule ModuleB do
    use Archeometer.Schema
    alias Archeometer.SchemaTest.ModuleA

    defschema :module_b do
      field(:second_module, primary_key: true)
      belongs_to(ModuleA, as: :other_a, key: :some_key)
      has(ModuleA)
    end
  end

  describe "schema fields generation" do
    test "every field is generated" do
      assert :first_module in ModuleA.__archeometer_fields__()
      assert :second_module in ModuleB.__archeometer_fields__()
    end
  end

  describe "schema associations generation" do
    test "associations with default values are well generated" do
      assert %Assoc{
               name: :module_b,
               keys: :module_b_id,
               type: :local,
               module: ModuleB
             } = ModuleA.__archeometer_assocs__()[:module_b]

      assert %Assoc{
               name: :module_a,
               keys: :module_b_id,
               type: :remote,
               module: ModuleA
             } = ModuleB.__archeometer_assocs__()[:module_a]
    end

    test "associations with custom values are well generated" do
      assert %Assoc{
               name: :other_b,
               keys: :recurse_id,
               type: :remote,
               module: ModuleB
             } = ModuleA.__archeometer_assocs__()[:other_b]

      assert %Assoc{
               name: :other_a,
               keys: :some_key,
               type: :local,
               module: ModuleA
             } = ModuleB.__archeometer_assocs__()[:other_a]
    end
  end

  describe "schema struct generation" do
    test "structs have the expected fields" do
      assert %ModuleA{
               first_module: nil,
               module_b: nil,
               other_b: nil
             } = struct(ModuleA)

      assert %ModuleB{
               second_module: nil,
               module_a: nil,
               other_a: nil
             } = struct(ModuleB)
    end
  end
end
