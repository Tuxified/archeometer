defmodule Archeometer.Query.SymbolTest do
  use ExUnit.Case
  use Archeometer.SchemaFixtures

  alias Archeometer.Query.{Term, Symbol, JoinExpr}
  alias Archeometer.Query

  defp initial_aliases_for(module),
    do: %{m: {:table_alias, [alias?: true, key?: true, dest: module]}}

  defp lookup_expr_alias_for(name),
    do: %{name => {:expr_alias, {:symb, [], [{name, [], nil}]}}}

  describe "symbol resolution" do
    test "with expression alias works" do
      ast = quote do: id
      {:ok, valid_ast} = Term.validate(ast)

      assert {:ok, {:symb, _, [{:id, _, _}]}} =
               Symbol.resolve_symbols(
                 valid_ast,
                 lookup_expr_alias_for(:id)
               )
    end

    test "with schema fields" do
      ast = quote do: m.id

      {:ok, valid_ast} = Term.validate(ast)

      assert {:ok, {:symb, _, [{:m, _, nil}, {:id, meta, nil}]}} =
               Symbol.resolve_symbols(
                 valid_ast,
                 initial_aliases_for(ModuleA)
               )

      assert {:source, ModuleA} in meta
    end

    test "with non existent field" do
      ast = quote do: like(m.not_here, m.module_b.id)

      {:ok, valid_ast} = Term.validate(ast)

      assert {:error, {:unresolved_symb, {:not_here, _source}}} =
               Symbol.resolve_symbols(valid_ast, initial_aliases_for(ModuleA))
    end

    test "with remote foreign key" do
      ast = quote do: m.module_b.id

      {:ok, valid_ast} = Term.validate(ast)

      assert {:ok, {:symb, _, [{:m, _, _}, {:module_b, meta, _}, {:id, _, _}]}} =
               Symbol.resolve_symbols(
                 valid_ast,
                 initial_aliases_for(ModuleA)
               )

      assert {:source, ModuleA} in meta
      assert {:dest, ModuleB} in meta
      assert {:on, {:id, :module_a_id}} in meta
    end

    test "with local foreign key" do
      ast = quote do: m.module_a.id

      {:ok, valid_ast} = Term.validate(ast)

      assert {:ok, {:symb, _, [{:m, _, _}, {:module_a, meta, _}, {:id, _, _}]}} =
               Symbol.resolve_symbols(
                 valid_ast,
                 initial_aliases_for(ModuleB)
               )

      assert {:source, ModuleB} in meta
      assert {:dest, ModuleA} in meta
      assert {:on, {:module_a_id, :id}} in meta
    end

    test "with nested remote foreign key" do
      ast = quote do: m.module_b.module_c.id

      {:ok, valid_ast} = Term.validate(ast)

      assert {
               :ok,
               {:symb, _, [_, {:module_b, meta1, _}, {:module_c, meta2, _} | _]}
             } = Symbol.resolve_symbols(valid_ast, initial_aliases_for(ModuleA))

      assert {:source, ModuleA} in meta1
      assert {:dest, ModuleB} in meta1
      assert {:on, {:id, :module_a_id}} in meta1

      assert {:source, ModuleB} in meta2
      assert {:dest, ModuleC} in meta2
      assert {:on, {:id, :module_b_id}} in meta2
    end

    test "with nested local foreign key" do
      ast = quote do: m.module_b.module_a.id

      {:ok, valid_ast} = Term.validate(ast)

      assert {
               :ok,
               {:symb, _, [_, {:module_b, meta1, _}, {:module_a, meta2, _} | _]}
             } = Symbol.resolve_symbols(valid_ast, initial_aliases_for(ModuleC))

      assert {:source, ModuleC} in meta1
      assert {:dest, ModuleB} in meta1
      assert {:on, {:module_b_id, :id}} in meta1

      assert {:source, ModuleB} in meta2
      assert {:dest, ModuleA} in meta2
      assert {:on, {:module_a_id, :id}} in meta2
    end

    test "with subqueries" do
      ast = quote do: m.module_b.id
      {:ok, valid_ast} = Term.validate(ast)

      subq = %{select: [[valid_ast]]}

      assert {:ok,
              %{select: [[{:symb, _, [{:m, _, nil}, {:module_b, meta, nil}, {:id, _, nil}]}]]}} =
               Symbol.resolve_symbols(
                 subq,
                 initial_aliases_for(ModuleA)
               )

      assert {:source, ModuleA} in meta
      assert {:dest, ModuleB} in meta
      assert {:on, {:id, :module_a_id}} in meta
    end

    test "with non existent nested key fails" do
      ast = quote do: like(m.module_b.id, m.module_b.not_here)

      {:ok, valid_ast} = Term.validate(ast)

      assert {:error, {:unresolved_symb, {:not_here, ModuleB}}} =
               Symbol.resolve_symbols(valid_ast, initial_aliases_for(ModuleA))
    end

    test "without alias prefix fails" do
      ast = quote do: like(module_b, module_b.not_here)

      {:ok, valid_ast} = Term.validate(ast)

      assert {:error, {:top_level_non_alias, _}} =
               Symbol.resolve_symbols(valid_ast, initial_aliases_for(ModuleA))
    end

    test "with only key fails" do
      {:ok, valid_ast} = Term.validate(quote do: m)

      assert {:error, {:table_not_selectable, _}} =
               Symbol.resolve_symbols(
                 valid_ast,
                 initial_aliases_for(ModuleA)
               )
    end

    test "with wrong key fails" do
      {:ok, valid_ast} = Term.validate(quote do: m.id.id)

      assert {:error, {:non_table_prefix, _}} =
               Symbol.resolve_symbols(
                 valid_ast,
                 initial_aliases_for(ModuleA)
               )
    end
  end

  defp alias_free_ast?({:symb, _, [{a, _, _}, {atom, _, _}]}) do
    case Atom.to_string(a) do
      "u" <> _ -> is_atom(atom)
      _ -> false
    end
  end

  defp alias_free_ast?({_, _, args}), do: Enum.all?(args, &alias_free_ast?/1)
  defp alias_free_ast?(_), do: true

  describe "symbols join tables aliases" do
    Module.put_attribute(
      __MODULE__,
      :lookups_to_alias,
      quote do
        [
          field: m.id,
          nested_key: m.module_b.id,
          nested_key: m.module_b.module_c.id,
          expr: like(m.module_b.id, m.id),
          expr: m.module_b.id + m.id,
          expr: m.module_b.id < 5 and m.module_b.module_c.id == 0,
          expr: m.module_b.id in ["Elixir"],
          string: "foo"
        ]
      end
    )

    for {kind, ast} <- @lookups_to_alias do
      test "#{kind} `#{Macro.to_string(ast)}` works" do
        init_query = Query.initial_query_for(ModuleA, alias: :m)
        {:ok, valid_ast} = Term.validate(unquote(Macro.escape(ast)))
        {:ok, resolved_ast} = Symbol.resolve_symbols(valid_ast, init_query.aliases)
        {_ast, alias_table} = JoinExpr.generate_joins(resolved_ast, init_query.tables)

        assert {:ok, alias_ast} = Symbol.replace_aliases(resolved_ast, alias_table)
        assert alias_free_ast?(alias_ast)
      end
    end
  end
end
