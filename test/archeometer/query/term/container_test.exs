defmodule Archeometer.Query.Term.ContainerTest do
  use ExUnit.Case
  alias Archeometer.Query.Term.Container

  describe "reduce" do
    test "with sigle expressions works" do
      Enum.each(0..10, fn i ->
        assert i == Container.reduce(i, 0, &(&1 + &2))
      end)
    end

    test "with tuples works" do
      Enum.each(0..10, fn i ->
        t =
          0..i
          |> Enum.to_list()
          |> List.to_tuple()
          |> Macro.escape()

        assert Enum.sum(0..i) == Container.reduce(t, 0, &(&1 + &2))
      end)
    end

    test "with lists works" do
      Enum.each(0..10, fn i ->
        t = Enum.to_list(0..i)

        assert Enum.sum(0..i) == Container.reduce(t, 0, &(&1 + &2))
      end)
    end

    test "with maps with atom keys works" do
      Enum.each(0..10, fn i ->
        t =
          0..i
          |> Map.new(fn j ->
            {j |> Integer.to_string() |> String.to_atom(), j}
          end)
          |> Macro.escape()

        assert Enum.sum(0..i) ==
                 Container.reduce(t, 0, fn {_k, v}, acc ->
                   v + acc
                 end)
      end)
    end

    test "with keyword lists works" do
      Enum.each(0..10, fn i ->
        t =
          Enum.map(
            0..i,
            fn j -> {j |> Integer.to_string() |> String.to_atom(), j} end
          )

        assert Enum.sum(0..i) ==
                 Container.reduce(t, 0, fn {_k, v}, acc ->
                   v + acc
                 end)
      end)
    end

    test "with maps with non-atom keys doesn't work" do
      Enum.each(0..10, fn i ->
        t =
          0..i
          |> Enum.to_list()
          |> Map.new(fn j ->
            {j, j}
          end)
          |> Macro.escape()

        assert {:error, {:invalid_keys, _}} =
                 Container.reduce(t, 0, fn {_k, v}, acc ->
                   v + acc
                 end)
      end)
    end
  end
end
