defmodule Archeometer.Collect.CredoIssueTest do
  use ExUnit.Case
  alias Archeometer.Collect.CredoIssues

  @test_project "test/resources/sample_proj"

  test "credo in project returns issues" do
    assert %{credo_issues: issues} =
             Mix.Project.in_project(
               :sample_proj,
               @test_project,
               fn _mod -> CredoIssues.collect() end
             )

    assert length(issues) == 3
  end
end
