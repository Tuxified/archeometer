defmodule Archeometer.Collect.ProjectTest do
  use ExUnit.Case
  alias Archeometer.Collect.Project

  test "filter dependencies" do
    filters = ["credo*", "elixir_uuid"]

    deps = Project.filter_deps(filters)
    assert is_list(deps)
    assert deps |> Keyword.keys() == [:credo, :elixir_uuid]
    assert deps |> Keyword.get(:credo) =~ "archeometer/deps/credo"
    assert deps |> Keyword.get(:elixir_uuid) =~ "archeometer/deps/elixir_uuid"
  end
end
