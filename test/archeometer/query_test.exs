defmodule Archeometer.QueryTest do
  use ExUnit.Case
  use Archeometer.SchemaFixtures
  import Archeometer.Query
  alias Archeometer.Schema.App

  doctest Archeometer.Query, import: true

  test "Inspect query" do
    query =
      from(a in App,
        select: [
          name: a.name,
          num_mods: count(a.modules.id),
          num_lines: sum(a.modules.num_lines)
        ],
        where: not exists(a.modules.macros.name),
        group_by: a.name,
        order_by: [desc: num_mods]
      )

    assert inspect(query) =~ "{:ok, Archeometer.Query.from"
  end
end
