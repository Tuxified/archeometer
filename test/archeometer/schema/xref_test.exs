defmodule Archeometer.Schema.XRefTest do
  use ExUnit.Case
  import Archeometer.Query
  alias Archeometer.Schema.XRef

  @test_db "test/resources/db/archeometer_jissai.db"

  test "list ecto associations" do
    query =
      from(x in XRef,
        select: [
          caller: x.caller.name,
          callee: x.callee.name,
          assoc: x.type
        ],
        where: like(x.type, "ecto%")
      )

    assert Archeometer.Repo.all(query, [], @test_db).rows == []
  end
end
