defmodule Archeometer.Schema.BehaviourTest do
  use ExUnit.Case
  import Archeometer.Query
  alias Archeometer.Schema.Behaviour

  @test_db "test/resources/db/archeometer_jissai.db"

  test "list modules with behaviours" do
    query =
      from(b in Behaviour,
        select: [
          name: b.name,
          module: b.module.name
        ]
      )

    result = Archeometer.Repo.all(query, [], @test_db)

    assert result.headers == [:name, :module]

    assert result.rows ==
             [
               ["Supervisor", "JissaiWeb.Telemetry"],
               ["OpenApiSpex.Schema", "JissaiWeb.StudentSchemas.StudentSimple"],
               ["OpenApiSpex.Schema", "JissaiWeb.StudentSchemas.StudentSessionDynamic"],
               ["OpenApiSpex.Schema", "JissaiWeb.StudentSchemas.StudentParticipation"],
               ["OpenApiSpex.Schema", "JissaiWeb.Sessions.AttendancesSchemas.SuccessResponse"],
               ["Plug", "JissaiWeb.Router"],
               ["Plug", "JissaiWeb.ReportController"],
               ["Plug", "JissaiWeb.RenderError"],
               ["OpenApiSpex.Schema", "JissaiWeb.ParticipationSchemas.Participation"],
               ["Plug", "JissaiWeb.PageController"],
               ["Gettext.Backend", "JissaiWeb.Gettext"],
               ["Plug", "JissaiWeb.FallbackController"],
               ["OpenApiSpex.Schema", "JissaiWeb.EntriesSchemas.Entry"],
               ["Phoenix.Endpoint", "JissaiWeb.Endpoint"],
               ["Plug", "JissaiWeb.Endpoint"],
               ["OpenApiSpex.Schema", "JissaiWeb.Courses.StudentsSchemas.SuccessResponse"],
               ["OpenApiSpex.Schema", "JissaiWeb.Courses.SessionDynamicsSchemas.SuccessResponse"],
               ["OpenApiSpex.Schema", "JissaiWeb.Courses.SessionDatesSchemas.SuccessResponse"],
               ["OpenApiSpex.Schema", "JissaiWeb.Courses.ParticipationSchemas.SuccessResponse"],
               [
                 "OpenApiSpex.Schema",
                 "JissaiWeb.Courses.AttendancesExportSchemas.SuccessResponse"
               ],
               ["OpenApiSpex.Schema", "JissaiWeb.ClassesSchemas.ClassSessionDynamicItem"],
               ["OpenApiSpex.Schema", "JissaiWeb.ClassesSchemas.ClassSessionDateItem"],
               ["OpenApiSpex.Schema", "JissaiWeb.ClassesSchemas.ClassItem"],
               ["OpenApiSpex.Schema", "JissaiWeb.ClassesSchemas.ClassAttendanceItem"],
               ["Application", "JissaiWeb.Application"],
               ["OpenApiSpex.OpenApi", "JissaiWeb.ApiSpec"],
               ["OpenApiSpex.Schema", "JissaiWeb.ActivitySchemas.Activity"],
               ["Ecto.Repo", "Jissai.Repo"],
               ["Quantum", "Jissai.DataAnalysis.AnalyticsScheduler"],
               ["Ecto.Repo", "Jissai.AtamaRepo"],
               ["Application", "Jissai.Application"],
               ["Exception", "Jissai.AnalyticsSessionNotFound"],
               ["Exception", "Jissai.AnalyticsNoInputs"],
               ["Exception", "Jissai.AnalyticsInvalidInputsSize"]
             ]
  end
end
