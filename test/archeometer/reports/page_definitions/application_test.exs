defmodule Archeometer.Reports.PageDefinitions.ApplicationTest do
  use ExUnit.Case

  alias Archeometer.Reports.Page
  alias Archeometer.Reports.PageDefinition.Application, as: AppDef

  @test_db "test/resources/db/archeometer_jissai.db"
  @app_name app: "jissai"
  @bindings [app: "jissai", limit: 10]

  # fragments ALWAYS use the default database, so we need to override it
  setup do
    default_db = Application.fetch_env!(:archeometer, :test_db)
    Application.put_env(:archeometer, :test_db, @test_db)

    on_exit(fn -> Application.put_env(:archeometer, :test_db, default_db) end)
  end

  @tag :graphviz
  test "page for app" do
    page_def = AppDef.definition(@app_name)
    %Page{sections: sections} = Page.process(page_def, @bindings)
    fragments = Enum.flat_map(sections, fn s -> s.fragments end)

    svg_fragments = Enum.filter(fragments, fn f -> f.result_type == :svg end)
    assert length(svg_fragments) == 3

    treemap_svg_fragments = Enum.filter(fragments, fn f -> f.result_type == :treemap_svg end)
    assert length(treemap_svg_fragments) == 1

    img_fragments = Enum.filter(fragments, fn f -> f.result_type == :image end)
    assert length(img_fragments) == 1

    (svg_fragments ++ treemap_svg_fragments ++ img_fragments)
    |> Enum.each(fn f ->
      ext = String.split(f.result, ".") |> List.last()
      assert valid_img?(f.result, ext)
    end)
  end

  defp valid_img?(path, ext) do
    File.regular?(path) and String.ends_with?(path, ext)
  end
end
