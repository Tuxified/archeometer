defmodule Archeometer.Reports.PageDefinitions.ProjectTest do
  use ExUnit.Case

  alias Archeometer.Reports.Page
  alias Archeometer.Reports.Fragment
  alias Archeometer.Reports.PageDefinition.Project
  alias Archeometer.Reports.FragmentDefs

  @project_name "jissai"
  @test_db "test/resources/db/archeometer_jissai.db"

  # fragments ALWAYS use the default database, so we need to override it
  setup do
    default_db = Application.fetch_env!(:archeometer, :test_db)
    Application.put_env(:archeometer, :test_db, @test_db)

    on_exit(fn -> Application.put_env(:archeometer, :test_db, default_db) end)
  end

  test "page for project" do
    page_def = Project.definition(@project_name)

    %Page{sections: sections} = Page.process(page_def, extra_deps: [])

    assert [f1, f2] = Enum.flat_map(sections, fn s -> s.fragments end)

    assert f1.result_type == :table
    assert f2.result_type == :svg

    assert %{
             headers: ["name", "num_mods", "num_lines", "num_ecto_schemas"],
             values: [
               ["jissai_web", 61, 3290, 0],
               ["jissai", 40, 6382, 4]
             ]
           } = f1.result
  end

  test "Validate CQl - SQL - List of applications and their module count" do
    fragment = FragmentDefs.Application.applications_and_modules()
    alt_code_res = Fragment.alt_process(fragment, app: "jissai") |> Map.get(:result)
    cql_res = Fragment.process(fragment, app: "jissai") |> Map.get(:result)
    assert alt_code_res == cql_res
  end
end
