defmodule Archeometer.Reports.FragmentDefsTest do
  use ExUnit.Case

  alias Archeometer.Reports.Fragment
  alias Archeometer.Reports.FragmentDefs
  alias Archeometer.Reports.Section

  @bindings [app: "jissai", limit: 10]
  @test_db "test/resources/db/archeometer_jissai.db"

  # fragments ALWAYS use the default database, so we need to override it
  setup do
    default_db = Application.fetch_env!(:archeometer, :test_db)
    Application.put_env(:archeometer, :test_db, @test_db)

    on_exit(fn -> Application.put_env(:archeometer, :test_db, default_db) end)
  end

  describe "Size" do
    test "largest modules" do
      fragment_def = FragmentDefs.Size.largest_modules()
      fragment = Fragment.process(fragment_def, @bindings)
      description = EEx.eval_string(fragment_def.desc, assigns: @bindings)

      assert fragment.desc == description
      assert is_map(fragment.result)
      assert length(fragment.result.values) == 10
    end

    test "Validate CQL - SQL - largest modules " do
      fragment_def = FragmentDefs.Size.largest_modules()

      alt_code_res = Fragment.alt_process(fragment_def, @bindings) |> Map.get(:result)

      cql_res = Fragment.process(fragment_def, @bindings) |> Map.get(:result)
      assert alt_code_res == cql_res
    end

    test "Treemap render" do
      fragment_def = FragmentDefs.Size.module_size_treemap()

      fragment = Fragment.process(fragment_def, @bindings)

      assert fragment.desc == fragment_def.desc
      assert valid_img?(fragment.result, ".svg")
    end
  end

  describe "Complexity" do
    test "most complex modules" do
      fragment_def = FragmentDefs.Complexity.most_complex_modules()
      fragment = Fragment.process(fragment_def, @bindings)
      description = EEx.eval_string(fragment_def.desc, assigns: @bindings)

      assert fragment.desc == description
      assert is_map(fragment.result)
      assert length(fragment.result.values) == 10
    end

    test "most complex functions" do
      fragment_def = FragmentDefs.Complexity.most_complex_functions()
      fragment = Fragment.process(fragment_def, @bindings)
      description = EEx.eval_string(fragment_def.desc, assigns: @bindings)

      assert fragment.desc == description
      assert is_map(fragment.result)
      assert length(fragment.result.values) == 10
    end

    test "Validate CQL - SQL - Most complex modules " do
      fragment_def = FragmentDefs.Complexity.most_complex_modules()

      alt_code_res = Fragment.alt_process(fragment_def, @bindings) |> Map.get(:result)

      cql_res = Fragment.process(fragment_def, @bindings) |> Map.get(:result)
      assert alt_code_res == cql_res
    end

    test "Validate CQL - SQL - Most complex functions " do
      fragment_def = FragmentDefs.Complexity.most_complex_functions()

      alt_code_res = Fragment.alt_process(fragment_def, @bindings) |> Map.get(:result)

      cql_res = Fragment.process(fragment_def, @bindings) |> Map.get(:result)
      assert alt_code_res == cql_res
    end
  end

  describe "Core modules" do
    test "modules with most dependencies" do
      fragment_def = FragmentDefs.Core.modules_with_most_deps()
      fragment = Fragment.process(fragment_def, @bindings)

      assert fragment.desc == fragment_def.desc
      assert is_map(fragment.result)
      assert length(fragment.result.values) == 10
    end

    test "core modules" do
      fragment_def = FragmentDefs.Core.core_modules()
      fragment = Fragment.process(fragment_def, @bindings)

      assert fragment.desc == fragment_def.desc
      assert is_map(fragment.result)
      assert length(fragment.result.values) == 4
    end

    @tag :graphviz
    test "core modules graph" do
      modules =
        [
          "Jissai.AtamaData",
          "Jissai.AttendanceProcess",
          "Jissai.ClassDynamicsProcess",
          "Jissai.DataAnalysis",
          "Jissai.ParticipationProcesss",
          "Jissai.ReportProvider",
          "Jissai.Reports",
          "Jissai.UserRoleHelper"
        ]
        |> Enum.join(" ")

      fragment_def = FragmentDefs.Core.core_modules_graph()
      fragment = Fragment.process(fragment_def, @bindings ++ [mod_names: modules])

      assert fragment.desc == fragment_def.desc
      assert valid_img?(fragment.result, ".svg")
    end

    test "Validate CQL - SQL - modules with most dependencies " do
      fragment_def = FragmentDefs.Core.modules_with_most_deps()

      alt_code_res = Fragment.alt_process(fragment_def, @bindings) |> Map.get(:result)

      cql_res = Fragment.process(fragment_def, @bindings) |> Map.get(:result)
      assert alt_code_res == cql_res
    end

    test "Validate CQL - SQL - core modules " do
      fragment_def = FragmentDefs.Core.core_modules()

      alt_code_res = Fragment.alt_process(fragment_def, @bindings) |> Map.get(:result)

      cql_res = Fragment.process(fragment_def, @bindings) |> Map.get(:result)
      assert alt_code_res == cql_res
    end
  end

  describe "Building blocks" do
    test "most used modules" do
      fragment_def = FragmentDefs.BuildingBlocks.most_used_modules()
      fragment = Fragment.process(fragment_def, @bindings)

      assert fragment.desc == fragment_def.desc
      assert is_map(fragment.result)
      assert length(fragment.result.values) == 10
    end

    test "Validate CQL - SQL - most used modules " do
      fragment_def = FragmentDefs.BuildingBlocks.most_used_modules()

      alt_code_res = Fragment.alt_process(fragment_def, @bindings) |> Map.get(:result)

      cql_res = Fragment.process(fragment_def, @bindings) |> Map.get(:result)
      assert alt_code_res == cql_res
    end
  end

  describe "Public API" do
    test "modules with most public funcitons" do
      fragment_def = FragmentDefs.API.modules_with_most_public_funs()
      fragment = Fragment.process(fragment_def, @bindings)

      assert fragment.desc == fragment_def.desc
      assert is_map(fragment.result)
      assert length(fragment.result.values) == 10
    end

    test "Validate CQL - SQL - modules with most public funcitons " do
      fragment_def = FragmentDefs.API.modules_with_most_public_funs()

      alt_code_res = Fragment.alt_process(fragment_def, @bindings) |> Map.get(:result)

      cql_res = Fragment.process(fragment_def, @bindings) |> Map.get(:result)
      assert alt_code_res == cql_res
    end
  end

  describe "Dependency cycles" do
    test "dsm matrix" do
      fragment_def = FragmentDefs.DepsAnalysis.dsm_matrix()
      fragment = Fragment.process(fragment_def, @bindings)
      assert valid_img?(fragment.result, ".svg")
    end

    test "The largest cyclic dependencies group" do
      fragment_def = FragmentDefs.DepsAnalysis.dsm_largest_deps_group()
      fragment = Fragment.process(fragment_def, @bindings)
      assert fragment.desc == fragment_def.desc
      assert is_map(fragment.result)
      assert length(fragment.result.values) == 8
    end

    test "DSM dependencies group" do
      modules =
        [
          "Jissai.AtamaData",
          "Jissai.AttendanceProcess",
          "Jissai.ClassDynamicsProcess",
          "Jissai.DataAnalysis",
          "Jissai.ParticipationProcesss",
          "Jissai.ReportProvider",
          "Jissai.Reports",
          "Jissai.UserRoleHelper"
        ]
        |> Enum.join(" ")

      fragment_def = FragmentDefs.DepsAnalysis.dsm_largest_deps_group_graph()
      fragment = Fragment.process(fragment_def, @bindings ++ [mod_names: modules])
      assert fragment.desc == fragment_def.desc
      assert valid_img?(fragment.result, ".svg")
    end

    @tag :graphviz
    test "cyclic deps graph" do
      modules =
        [
          "Jissai.AtamaData",
          "Jissai.UserRoleHelper"
        ]
        |> Enum.join(" ")

      fragment_def = FragmentDefs.DepsAnalysis.deps_graph()
      fragment = Fragment.process(fragment_def, @bindings ++ [mod_names: modules])

      assert fragment.desc == fragment_def.desc
      assert valid_img?(fragment.result, ".svg")
    end
  end

  describe "Test coverage" do
    test "modules with least coverage" do
      fragment_def = FragmentDefs.TestCoverage.modules_with_least_coverage()
      fragment = Fragment.process(fragment_def, @bindings)

      assert fragment.desc == fragment_def.desc
      assert is_map(fragment.result)
      assert length(fragment.result.values) == 10
    end

    test "Validate CQL - SQL - modules with least coverage " do
      fragment_def = FragmentDefs.TestCoverage.modules_with_least_coverage()

      alt_code_res = Fragment.alt_process(fragment_def, @bindings) |> Map.get(:result)

      cql_res = Fragment.process(fragment_def, @bindings) |> Map.get(:result)
      assert alt_code_res == cql_res
    end
  end

  describe "Chained fragments" do
    @tag :graphviz
    test "core modules and their graph" do
      section = %Section.Definition{
        id: :core,
        desc: "Core modules",
        fragments: [
          FragmentDefs.Core.core_modules(),
          FragmentDefs.Core.core_modules_graph()
        ]
      }

      %Section{fragments: [_f1, f2]} = Section.process(section, @bindings)
      assert valid_img?(f2.result, ".svg")
    end
  end

  defp valid_img?(path, ext) do
    File.regular?(path) and String.ends_with?(path, ext)
  end
end
