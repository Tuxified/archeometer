defmodule Archeometer.Reports.FragmentTest do
  use ExUnit.Case
  alias Archeometer.Reports.Fragment
  alias Archeometer.Reports.Fragment.Definition

  @test_db "test/resources/db/archeometer_jissai.db"

  # fragments ALWAYS use the default database, so we need to override i
  setup do
    default_db = Application.fetch_env!(:archeometer, :test_db)
    Application.put_env(:archeometer, :test_db, @test_db)

    on_exit(fn -> Application.put_env(:archeometer, :test_db, default_db) end)
  end

  test "simple parametrized query" do
    cql_template = """
    Repo.all(
      from m in Module,
        select: [
          name: m.name,
          num_lines: m.num_lines
        ],
        order_by: [desc: num_lines],
        where: m.app.name == "<%= @app %>",
        limit: 10
    )
    """

    alt_code_template = """
    select a.name, count(m.id) num_mods, sum(m.num_lines) num_lines
    from apps a
    inner join modules m on a.id = m.app_id
    group by a.name
    order by num_mods desc;
    """

    fragment_def = %Definition{
      query_type: :cql,
      result_type: :table,
      desc: "Example CQL fragment",
      code: cql_template,
      alt_code: alt_code_template
    }

    assert %Fragment{code: code, result: result} =
             Fragment.process(
               fragment_def,
               app: "jissai"
             )

    assert code != cql_template
    refute Enum.empty?(result)
  end

  test "dsm image fragment" do
    mix_cmd = "mix arch.dsm --format svg --out <%=@fname%> Jissai"

    fragment_def = %Definition{
      query_type: :mix_task,
      result_type: :image,
      desc: "Example mix task fragment. Ouputs an image",
      code: mix_cmd
    }

    assert %Fragment{code: code, result: result} =
             Fragment.process(
               fragment_def,
               # App names starting with ':' must be fixed
               app: "jissai"
             )

    assert code != mix_cmd
    assert String.ends_with?(result, ".svg")
    assert File.regular?(result)
  end

  describe "cli command fragments" do
    test "png" do
      cmd = "echo 'digraph { a -> b }' | dot -Tpng > <%= @fname %>"

      fragment_def = %Definition{
        query_type: :cli_command,
        result_type: :image,
        desc: "Example cli fragment. Ouputs a PNG image",
        code: cmd
      }

      assert %Fragment{code: code, result: result} =
               Fragment.process(
                 fragment_def,
                 # App names starting with ':' must be fixed
                 app: "jissai"
               )

      assert code != cmd
      assert String.ends_with?(result, ".png")
      assert File.regular?(result)
    end

    test "svg" do
      cmd = "echo 'digraph { a -> b }' | dot -Tsvg > <%= @fname %>"

      fragment_def = %Definition{
        query_type: :cli_command,
        result_type: :treemap_svg,
        desc: "Example cli fragment. Ouputs an SVG image",
        code: cmd
      }

      assert %Fragment{code: code, result: result} =
               Fragment.process(
                 fragment_def,
                 # App names starting with ':' must be fixed
                 app: "jissai"
               )

      assert code != cmd
      assert String.ends_with?(result, ".svg")
      assert File.regular?(result)
    end
  end
end
