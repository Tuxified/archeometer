defmodule Archeometer.Reports.Render.HtmlTest do
  use ExUnit.Case

  alias Archeometer.Reports.Page
  alias Archeometer.Reports.Section
  alias Archeometer.Reports.Render
  alias Archeometer.Reports.PageDefinition

  @test_db "test/resources/db/archeometer_jissai.db"
  @app_name "jissai"
  @bindings [app: "jissai", limit: 10]

  # fragments ALWAYS use the default database, so we need to override it
  setup do
    default_db = Application.fetch_env!(:archeometer, :test_db)
    Application.put_env(:archeometer, :test_db, @test_db)

    on_exit(fn -> Application.put_env(:archeometer, :test_db, default_db) end)
  end

  test "single app page" do
    html =
      app_page_def()
      |> Page.process(@bindings)
      |> Render.Html.render([:index], Render.Html)

    assert valid_html?(html)
  end

  test "umbrella project page" do
    html =
      project_page_def()
      |> Page.process(@bindings)
      |> Render.Html.render([{:index, ["Project Structure"]}, {:jissai, ["Size"]}], Render.Html)

    assert valid_html?(html)
  end

  defp app_page_def() do
    PageDefinition.Application.definition(@app_name)
  end

  defp project_page_def() do
    # We can't invoke 'mix arch.apps.xref' within tests, so we remove the corresponding fragment.
    %Page.Definition{
      id: name,
      sections: [
        %Section.Definition{
          id: section_id,
          desc: desc,
          fragments: [apps_and_modules, _]
        }
      ]
    } = PageDefinition.Project.definition(@app_name)

    %Page.Definition{
      id: name,
      sections: [
        %Section.Definition{
          id: section_id,
          desc: desc,
          fragments: [apps_and_modules]
        }
      ]
    }
  end

  defp valid_html?(str) do
    assert String.starts_with?(str, "<!DOCTYPE html><html><head><title>Archeometer</title>")
    assert String.ends_with?(str, "</body></html>")
  end
end
