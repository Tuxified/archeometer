defmodule Archeometer.Reports.PageTest do
  use ExUnit.Case

  alias Archeometer.Reports.Fragment
  alias Archeometer.Reports.Section
  alias Archeometer.Reports.Page

  @test_db "test/resources/db/archeometer_jissai.db"

  # fragments ALWAYS use the default database, so we need to override it
  setup do
    default_db = Application.fetch_env!(:archeometer, :test_db)
    Application.put_env(:archeometer, :test_db, @test_db)

    on_exit(fn -> Application.put_env(:archeometer, :test_db, default_db) end)
  end

  test "simple page with 2 sections" do
    page_def = %Page.Definition{
      id: :my_app,
      sections: [
        %Section.Definition{
          id: :section_1,
          desc: "Section 1",
          fragments: [cql_fragment(), mix_task_fragment()]
        },
        %Section.Definition{
          id: :section_2,
          desc: "Section 2",
          fragments: [mix_task_fragment(), cql_fragment()]
        }
      ]
    }

    page = %Page{} = Page.process(page_def, app: "jissai")

    assert [s1 = %Section{}, s2 = %Section{}] = page.sections
    assert [s1_f1 = %Fragment{}, s1_f2 = %Fragment{}] = s1.fragments
    assert [s2_f1 = %Fragment{}, s2_f2 = %Fragment{}] = s2.fragments
    assert valid_img?(s1_f2.result, ".svg")
    assert valid_img?(s2_f1.result, ".svg")
    assert s1_f2.result != s2_f1.result
    assert s1_f1.result == s2_f2.result
  end

  defp valid_img?(path, ext) do
    File.regular?(path) and String.ends_with?(path, ext)
  end

  defp cql_fragment() do
    cql_template = """
    Repo.all(
      from m in Module,
        select: [
          name: m.name,
          num_lines: m.num_lines
        ],
        order_by: [desc: num_lines],
        where: m.app.name == "<%= @app %>",
        limit: 10
    )
    """

    alt_code_template = """
    select a.name, count(m.id) num_mods, sum(m.num_lines) num_lines
    from apps a
    inner join modules m on a.id = m.app_id
    group by a.name
    order by num_mods desc;
    """

    %Fragment.Definition{
      query_type: :cql,
      result_type: :table,
      result_name: :none,
      desc: "Example CQL fragment",
      code: cql_template,
      alt_code: alt_code_template
    }
  end

  defp mix_task_fragment() do
    mix_cmd = "mix arch.dsm --format svg --out <%=@fname%> Jissai"

    %Fragment.Definition{
      query_type: :mix_task,
      result_type: :image,
      result_name: :none,
      desc: "Example mix task fragment. Ouputs an image",
      code: mix_cmd
    }
  end
end
