defmodule Archeometer.Analysis.Treemap.DataTest do
  use ExUnit.Case

  alias Archeometer.Analysis.Treemap.Node
  alias Archeometer.Analysis.Treemap
  alias Archeometer.Analysis.Treemap.SVGRender

  @sample_proj_path "test/resources/sample_proj"
  @db_path "./test/resources/db/archeometer_jissai.db"
  @jissai_modules 25
  @jissai_test_modules 14
  @jissai_web_modules 52
  @jissai_test_modules_w_functions 9
  @jissai_modules_w_functions 17
  @jissai_web_modules_w_functions 36

  describe "create treemap of size from db" do
    test "all modules within an application, including tests" do
      tree = Treemap.treemap(:size, app: "jissai", db: @db_path, skip_tests: false)
      file = SVGRender.render(tree)
      lines = String.split(file, "\n")

      assert count_modules(lines, "Jissai") == @jissai_modules + @jissai_test_modules
    end

    test "all non test related modules within an application" do
      tree = Treemap.treemap(:size, app: "jissai", db: @db_path, skip_tests: true)
      file = SVGRender.render(tree)
      lines = String.split(file, "\n")

      assert count_modules(lines, "Jissai") == @jissai_modules
    end

    test "modules without specifying the app name" do
      tree = Treemap.treemap(:size, db: @db_path, skip_tests: true)
      file = SVGRender.render(tree)
      lines = String.split(file, "\n")

      assert count_modules(lines, "Jissai") == @jissai_modules + @jissai_web_modules
    end

    test "a module only by it's name" do
      tree =
        Treemap.treemap(:size, namespace: "Jissai.Application", db: @db_path, skip_tests: true)

      assert [
               %Node{
                 name: "Jissai.Application",
                 pct: 100.0
               }
             ] = tree.nodes
    end

    test "an especific module from a particular app" do
      tree =
        Treemap.treemap(
          :size,
          app: "jissai",
          namespace: "Jissai.Application",
          db: @db_path,
          skip_tests: true
        )

      assert [
               %Node{
                 name: "Jissai.Application",
                 pct: 100.0
               }
             ] = tree.nodes
    end
  end

  describe "create treemap of functions from db" do
    test "all functions within an application, including tests" do
      tree = Treemap.treemap(:functions, app: "jissai", db: @db_path, skip_tests: false)
      file = SVGRender.render(tree)
      lines = String.split(file, "\n")

      assert count_modules(lines, "Jissai") ==
               @jissai_modules_w_functions + @jissai_test_modules_w_functions
    end

    test "all non test related functions within an application" do
      tree = Treemap.treemap(:functions, app: "jissai", db: @db_path, skip_tests: true)
      file = SVGRender.render(tree)
      lines = String.split(file, "\n")

      assert count_modules(lines, "Jissai") == @jissai_modules_w_functions
    end

    test "functions without specifying the app name" do
      tree = Treemap.treemap(:functions, db: @db_path, skip_tests: true)
      file = SVGRender.render(tree)
      lines = String.split(file, "\n")

      assert count_modules(lines, "Jissai") ==
               @jissai_modules_w_functions + @jissai_web_modules_w_functions
    end

    test "a module only by it's name" do
      tree =
        Treemap.treemap(:functions,
          namespace: "Jissai.Application",
          db: @db_path,
          skip_tests: true
        )

      assert [
               %Node{
                 name: "Jissai.Application",
                 pct: 100.0
               }
             ] = tree.nodes
    end

    test "an especific module from a particular app" do
      tree =
        Treemap.treemap(
          :functions,
          app: "jissai",
          namespace: "Jissai.Application",
          db: @db_path,
          skip_tests: true
        )

      assert [
               %Node{
                 name: "Jissai.Application",
                 pct: 100.0
               }
             ] = tree.nodes
    end
  end

  describe "create treemap of issues from db" do
    setup do
      db = Path.join(@sample_proj_path, "archeometer_sample_proj.db")
      if not Archeometer.Repo.db_ready?(:basic, db), do: run_collect_in_sample_prj()
      [db: db]
    end

    test "all functions within an application, including tests", %{db: db} do
      tree = Treemap.treemap(:issues, app: "sample_proj", db: db, skip_tests: false)
      file = SVGRender.render(tree)
      lines = String.split(file, "\n")

      assert count_modules(lines, "SampleProj") == 2
    end

    test "without specifying the app name", %{db: db} do
      tree = Treemap.treemap(:issues, db: db, skip_tests: true)
      file = SVGRender.render(tree)
      lines = String.split(file, "\n")

      assert count_modules(lines, "SampleProj") == 2
    end

    test "a module only by it's name", %{db: db} do
      tree =
        Treemap.treemap(:issues,
          namespace: "SampleProj.Foo",
          db: db,
          skip_tests: true
        )

      assert [
               %Node{
                 name: "SampleProj.Foo",
                 pct: 100.0
               }
             ] = tree.nodes
    end

    test "an especific module from a particular app", %{db: db} do
      tree =
        Treemap.treemap(
          :issues,
          app: "sample_proj",
          namespace: "SampleProj.Foo",
          db: db,
          skip_tests: true
        )

      assert [
               %Node{
                 name: "SampleProj.Foo",
                 pct: 100.0
               }
             ] = tree.nodes
    end
  end

  test "Treemap with given modules" do
    modules = [
      {21, "Jissai.Reports", 361},
      {22, "Jissai.Reports.Attendance", 21},
      {23, "Jissai.Reports.ClassDynamic", 22},
      {24, "Jissai.Reports.Participant", 23}
    ]

    treemap = Treemap.treemap(:size, modules: modules)

    assert treemap.nodes ==
             [
               %Archeometer.Analysis.Treemap.Node{
                 kind: :group,
                 name: "Jissai.Reports",
                 nodes: [
                   %Archeometer.Analysis.Treemap.Node{
                     kind: :leaf,
                     name: "Jissai.Reports",
                     nodes: [],
                     pct: 84.54332552693208,
                     val: 361
                   },
                   %Archeometer.Analysis.Treemap.Node{
                     kind: :leaf,
                     name: "Jissai.Reports.Attendance",
                     nodes: [],
                     pct: 4.918032786885246,
                     val: 21
                   },
                   %Archeometer.Analysis.Treemap.Node{
                     kind: :leaf,
                     name: "Jissai.Reports.ClassDynamic",
                     nodes: [],
                     pct: 5.152224824355972,
                     val: 22
                   },
                   %Archeometer.Analysis.Treemap.Node{
                     kind: :leaf,
                     name: "Jissai.Reports.Participant",
                     nodes: [],
                     pct: 5.386416861826698,
                     val: 23
                   }
                 ],
                 pct: 100.0,
                 val: 427
               }
             ]
  end

  test "Treemap with incompatible options" do
    assert Treemap.treemap(:size, modules: [], app: :jissai) ==
             {:error, :incompatible_options}
  end

  defp count_modules(lines, app) do
    {:ok, rx} = Regex.compile("\\s+<title>#{app}.*</title>")
    Enum.count(lines, fn line -> Regex.match?(rx, line) end)
  end

  defp run_collect_in_sample_prj() do
    cwd = File.cwd!()
    File.cd(@sample_proj_path)
    Mix.Shell.cmd("mix arch.collect.static", [quiet: true], & &1)
    Mix.Shell.cmd("mix arch.collect.apps", [quiet: true], & &1)
    Mix.Shell.cmd("mix arch.collect.credo_issues", [quiet: true], & &1)
    File.cd(cwd)
  end
end
