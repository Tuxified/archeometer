defmodule Archeometer.Analysis.Treemap.SVGRenderTest do
  use ExUnit.Case

  alias Archeometer.Analysis.Treemap.SVGRender

  @treemap %{
    kind: :group,
    name: "Test",
    nodes: [
      %{
        kind: :leaf,
        name: "Test.ModuleA",
        nodes: [],
        pct: 20.0,
        val: 50
      },
      %{
        kind: :leaf,
        name: "Test.ModuleB",
        nodes: [],
        pct: 40.0,
        val: 100
      },
      %{
        kind: :leaf,
        name: "Test.ModuleC",
        nodes: [],
        pct: 20.0,
        val: 50
      },
      %{
        kind: :group,
        name: "Test.SubGroup",
        nodes: [
          %{
            kind: :group,
            name: "Test.SubGroup.SubGroup",
            nodes: [
              %{
                kind: :leaf,
                name: "Test.ModuleD",
                nodes: [],
                pct: 20.0,
                val: 50
              }
            ],
            pct: 20.0,
            val: 50
          }
        ],
        pct: 20.0,
        val: 50
      }
    ],
    pct: 100,
    val: 250
  }

  test "render/1 returns an error when root node is not a group" do
    node = %{
      kind: :leaf,
      name: "Test.ModuleD",
      nodes: [],
      pct: 100,
      val: 50
    }

    assert SVGRender.render(node) == {:error, "Root node must be a group"}
  end

  test "render/1 returns an error when root node is not equal to %100" do
    node = %{
      kind: :group,
      name: "Test.ModuleD",
      nodes: [],
      pct: 20.0,
      val: 50
    }

    assert SVGRender.render(node) == {:error, "Root node must equal to 100%"}
  end

  describe "svg" do
    test "has correct and no repeated tooltip" do
      file = SVGRender.render(@treemap)
      lines = String.split(file, "\n")
      modules = find_leaves(@treemap)

      # cheking no repeated tooltips

      Enum.each(modules, fn module ->
        assert has_tooltip?(lines, module)
      end)
    end

    test "has no negative dimensions" do
      file = SVGRender.render(@treemap)
      lines = String.split(file, "\n")

      # cheking no negative dimensions in rectangles of treemap

      assert has_negative_dimension?(lines) == false
    end
  end

  defp has_tooltip?(lines, {module, pct, val}) do
    {:ok, rx} = Regex.compile("^[[:space:]]+<title>#{module} - #{pct}\% - value: #{val}</title>")
    Enum.count(lines, fn line -> Regex.match?(rx, line) end) == 1
  end

  defp has_negative_dimension?(lines) do
    {:ok, rx} = Regex.compile(".+\"-[0-9]")
    Enum.any?(lines, fn line -> Regex.match?(rx, line) end)
  end

  defp find_leaves(tree) do
    Enum.map(tree.nodes, fn node ->
      if node.kind == :leaf do
        {node.name, node.pct, node.val}
      else
        [data] = find_leaves(node)
        data
      end
    end)
  end
end
