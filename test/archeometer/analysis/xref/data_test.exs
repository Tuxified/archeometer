defmodule Archeometer.Analysis.Xref.DataTest do
  use ExUnit.Case

  alias Archeometer.Analysis.Xref.Data

  @test_db "test/resources/db/archeometer_jissai.db"
  @jissai_total_modules 101
  @jissai_web_modules 61

  describe "Data.get_data" do
    test "get all modules in project" do
      modules = Data.get_modules(:none, "*", @test_db)

      assert Enum.count(modules) == @jissai_total_modules
    end

    test "get modules by namespace" do
      modules = Data.get_modules(:none, "Jissai.Reports", @test_db)

      assert modules == [
               ["Jissai.Reports"],
               ["Jissai.Reports.Attendance"],
               ["Jissai.Reports.ClassDynamic"],
               ["Jissai.Reports.Participant"]
             ]
    end

    test "get all modules by app name" do
      modules = Data.get_modules("jissai_web", "*", @test_db)

      assert Enum.count(modules) == @jissai_web_modules
    end

    test "get all modules by app and namespace" do
      modules = Data.get_modules("jissai_web", "JissaiWeb.ClassesSchemas", @test_db)

      assert modules == [
               ["JissaiWeb.ClassesSchemas.ClassSessionDynamicItem"],
               ["JissaiWeb.ClassesSchemas.ClassAttendanceItem"],
               ["JissaiWeb.ClassesSchemas.ClassSessionDateItem"],
               ["JissaiWeb.ClassesSchemas.ClassItem"],
               ["JissaiWeb.ClassesSchemas"]
             ]
    end
  end

  describe "Data.neighbors" do
    test "get incoming neighbors" do
      %{rows: incoming} = Data.neighbors("Jissai.AnalyticsBuilder", :incoming, @test_db)

      assert Enum.uniq(incoming) == [
               ["Jissai.AnalyticsWorker"],
               ["Jissai.UserRoleHelper"],
               ["Jissai.Reports"],
               ["Jissai.AnalyticsBuilder"]
             ]
    end

    test "get outgoing neighbors" do
      %{rows: outgoing} = Data.neighbors("Jissai.AnalyticsWorker", :outgoing, @test_db)

      assert Enum.uniq(outgoing) == [
               ["Jissai.DataAnalysisHelper"],
               ["Jissai.AtamaData"],
               ["Jissai.DataAnalysis"],
               ["Jissai.AnalyticsBuilder"]
             ]
    end

    test "get incoming and outgoing neighbors" do
      %{rows: neighbors} = Data.neighbors("Jissai.AttendanceProcess", :both, @test_db)

      assert Enum.uniq(neighbors) == [
               ["Jissai.AttendanceProcess"],
               ["Jissai.AnalyticsBuilder"],
               ["Jissai.AtamaData"],
               ["Jissai.DataAnalysis"],
               ["Jissai.AnalyticsNoInputs"]
             ]
    end
  end

  describe "Data.module_exists?" do
    test "when module exists" do
      assert Data.module_exists?("Jissai.Application", @test_db)
    end

    test "when module doesn't exist" do
      assert not Data.module_exists?("NonExistentModule", @test_db)
    end
  end
end
