defmodule Archeometer.Analysis.XrefTest do
  use ExUnit.Case

  alias Archeometer.Analysis.Xref
  doctest Xref, import: true

  @test_db "test/resources/db/archeometer_jissai.db"
  @modules ["Jissai.AtamaData", "Jissai.AtamaRepo"]

  test "Xref.gen_graph with dot format" do
    assert Xref.gen_graph(@modules, "dot", @test_db) =~
             Regex.compile!("""
             digraph G {
               .*;
               .*;
                 "Jissai.AtamaData" .*;
                 "Jissai.AtamaRepo" .*;
                   "Jissai.AtamaData" -> "Jissai.AtamaRepo";
             }
             """)
  end

  test "Xref.gen_graph with the default argument for database" do
    assert Xref.gen_graph(@modules, "dot", @test_db) =~
             Regex.compile!("""
             digraph G {
               .*;
               .*;
                 "Jissai.AtamaData" .*;
                 "Jissai.AtamaRepo" .*;
                   "Jissai.AtamaData" -> "Jissai.AtamaRepo";
             }
             """)
  end

  @tag :graphviz
  test "Xref.gen_graph with png format" do
    assert {:ok, {:image, :png}} ==
             @modules
             |> Xref.gen_graph("png", @test_db)
             |> MagicNumber.detect()
  end

  test "Xref.gen_graph with mermaid format" do
    assert Xref.gen_graph(@modules, "mermaid", @test_db) ==
             """
             graph TD;
               id_0([Jissai.AtamaData]);
               id_1([Jissai.AtamaRepo]);
               id_0-->id_1;
             """
  end

  test "Xref.gen_graph with unsupported format" do
    assert Xref.gen_graph(@modules, "Wrong.Format", @test_db) == {:error, :unsupported_format}
  end

  test "Xref.gen_graph with non existing database" do
    assert Xref.gen_graph(@modules, "dot", "Non.Existing.DB") ==
             {:error, :no_modules_matched}

    System.cmd("rm", ["Non.Existing.DB"])
  end

  test "Xref.gen_graph with a non existing module" do
    assert Xref.gen_graph(["Non.Existent.Module"], "dot", @test_db) ==
             {:error, "unknown module 'Non.Existent.Module'"}
  end

  test "Xref.gen_graph with app name and namespace" do
    assert Xref.gen_graph({"jissai", "Jissai.Reports"}, "dot", @test_db) =~
             Regex.compile!("""
             digraph G {
               .*;
               .*;
                 "Jissai.Reports" .*;
                 "Jissai.Reports.Attendance" .*;
                 "Jissai.Reports.ClassDynamic" .*;
                 "Jissai.Reports.Participant" .*;
                   "Jissai.Reports" -> "Jissai.Reports.Attendance";
                   "Jissai.Reports" -> "Jissai.Reports.Participant";
                   "Jissai.Reports" -> "Jissai.Reports.ClassDynamic";
             }
             """)
  end

  test "Xref.gen_graph with origin and dest" do
    assert Xref.gen_graph({["Jissai.AnalyticsWorker"], :outgoing}, "dot", @test_db) =~
             Regex.compile!("""
             digraph G {
               .*;
               .*;
                 "Jissai.AnalyticsBuilder" .*;
                 "Jissai.AnalyticsWorker" .*;
                 "Jissai.AtamaData" .*;
                 "Jissai.DataAnalysis" .*;
                 "Jissai.DataAnalysisHelper" .*;
                   "Jissai.AnalyticsWorker" -> "Jissai.DataAnalysisHelper";
                   "Jissai.AnalyticsWorker" -> "Jissai.AtamaData";
                   "Jissai.AnalyticsWorker" -> "Jissai.DataAnalysis";
                   "Jissai.AnalyticsWorker" -> "Jissai.AnalyticsBuilder";
                   "Jissai.AtamaData" -> "Jissai.DataAnalysisHelper";
                   "Jissai.DataAnalysis" -> "Jissai.AtamaData";
                   "Jissai.DataAnalysisHelper" -> "Jissai.DataAnalysis";
             }
             """)

    assert Xref.gen_graph({["JissaiWeb.ErrorHelpers"], :incoming}, "dot", @test_db) =~
             Regex.compile!("""
             digraph G {
               .*;
               .*;
                 "JissaiWeb.ChangesetView" .*;
                 "JissaiWeb.CsvView" .*;
                 "JissaiWeb.ErrorHelpers" .*;
                 "JissaiWeb.ErrorView" .*;
                 "JissaiWeb.LayoutView" .*;
                 "JissaiWeb.PageView" .*;
                 "JissaiWeb.ReportView" .*;
                 "JissaiWeb.StudentView" .*;
                   "JissaiWeb.ChangesetView" -> "JissaiWeb.ErrorHelpers";
                   "JissaiWeb.CsvView" -> "JissaiWeb.ErrorHelpers";
                   "JissaiWeb.ErrorView" -> "JissaiWeb.ErrorHelpers";
                   "JissaiWeb.LayoutView" -> "JissaiWeb.ErrorHelpers";
                   "JissaiWeb.PageView" -> "JissaiWeb.ErrorHelpers";
                   "JissaiWeb.ReportView" -> "JissaiWeb.ErrorHelpers";
                   "JissaiWeb.StudentView" -> "JissaiWeb.ErrorHelpers";
             }
             """)
  end

  describe "direct rendering" do
    test "png" do
      s = Xref.render(%{"a" => ["b"], "b" => ["a"]}, "png")
      assert is_binary(s)
      assert {:ok, {:image, :png}} = MagicNumber.detect(s)
    end

    test "svg" do
      s = Xref.render(%{"a" => ["b"], "b" => ["a"]}, "svg")
      assert is_binary(s)
      assert s =~ "<?xml"
    end
  end
end
