defmodule Archeometer.Analysis.DSMTest do
  use ExUnit.Case
  doctest Archeometer.Analysis.DSM

  alias Archeometer.Analysis.DSM

  @non_cyclic_dsm %{
                    1 => [],
                    2 => [1, 3],
                    3 => [1],
                    4 => [2, 3]
                  }
                  |> DSM.gen_dsm()

  @cyclic_mini_dsm %{
                     1 => [4],
                     2 => [1],
                     3 => [1, 2],
                     4 => [2]
                   }
                   |> DSM.gen_dsm()

  @cyclic_dsm %{
                1 => [8, 12],
                2 => [3, 6, 7, 10, 11],
                3 => [4, 9, 10, 11],
                4 => [8],
                5 => [4],
                6 => [4, 5, 9, 10],
                7 => [],
                8 => [5],
                9 => [8, 12],
                10 => [9, 12],
                11 => [5, 7, 8, 10, 12],
                12 => [4, 6, 10]
              }
              |> DSM.gen_dsm()

  @dsm_names %{
    1 => "one",
    2 => "two",
    3 => "three",
    4 => "four",
    5 => "five",
    6 => "six",
    7 => "seven",
    8 => "eight",
    9 => "nine",
    10 => "ten",
    11 => "eleven",
    12 => "twelve"
  }

  describe "DSM creation" do
    test "can create DSM from xrefs" do
      assert DSM.gen_dsm(%{1 => [2], 2 => [1, 3], 3 => [2]}) == %DSM{
               edges: %{
                 {1, 1} => :+,
                 {1, 2} => :*,
                 {2, 1} => :*,
                 {2, 2} => :+,
                 {2, 3} => :*,
                 {3, 2} => :*,
                 {3, 3} => :+
               },
               groups: %{},
               nodes: [1, 2, 3]
             }
    end
  end

  describe "basic functions" do
    test "root nodes" do
      assert DSM.roots(@cyclic_dsm) == [1, 2]
    end

    test "leaf nodes" do
      assert DSM.leaves(@cyclic_dsm) == [7]
    end

    test "find cycle" do
      assert DSM.find_cycle(@cyclic_mini_dsm) == [1, 4, 2]
    end
  end

  describe "triangularize" do
    test "triangularizable DSM is correctly triangularized" do
      assert %DSM{nodes: [4, 2, 3, 1], groups: %{}} = DSM.triangularize(@non_cyclic_dsm)
    end

    test "cyclic DSM is correctly triangularized" do
      seq = [1, 2, 3, 11, 6, 9, 12, 10, 4, 8, 5, 7]
      g1 = [6, 9, 12]
      g2 = [4, 5, 8]

      assert(
        %DSM{nodes: ^seq, groups: %{"g1" => ^g1, "g2" => ^g2}} = DSM.triangularize(@cyclic_dsm)
      )
    end
  end

  describe "pretty print" do
    alias Archeometer.Analysis.DSM.ConsoleRender, as: Render

    test "prints original (non triangularized) dsm to console" do
      output = Render.render(@cyclic_dsm, @dsm_names)

      assert String.contains?(output, "Design Structure Matrix (DSM):")
      assert String.contains?(output, "Modules (12):")
      assert String.contains?(output, "No cycles!")
    end

    test "prints triangularized dsm with cycles to console" do
      output = @cyclic_dsm |> DSM.triangularize() |> Render.render(@dsm_names)

      assert String.contains?(output, "Design Structure Matrix (DSM):")
      assert String.contains?(output, "Modules (12):")
      assert String.contains?(output, "Cycles (3):")
    end

    test "prints triangularized dsm and cyclic dependencies groups to console" do
      output =
        @cyclic_dsm
        |> DSM.triangularize()
        |> DSM.cyclic_deps_groups()
        |> Render.render(@dsm_names)

      assert String.contains?(output, "Design Structure Matrix (DSM):")
      assert String.contains?(output, "Modules (12):")
      assert String.contains?(output, "Group 1: six, nine, twelve")
    end

    test "prints triangularized dsm without cyclic dependencies groups to console" do
      output =
        @non_cyclic_dsm
        |> DSM.triangularize()
        |> DSM.cyclic_deps_groups()
        |> Render.render(@dsm_names)

      assert String.contains?(output, "Design Structure Matrix (DSM):")
      assert String.contains?(output, "Modules (4):")
      assert String.contains?(output, "No cycles!")
    end

    test "prints triangularized dsm without cycles to console" do
      output = @non_cyclic_dsm |> DSM.triangularize() |> Render.render(@dsm_names)

      assert String.contains?(output, "Design Structure Matrix (DSM):")
      assert String.contains?(output, "Modules (4):")
      assert String.contains?(output, "No cycles!")
    end
  end
end
