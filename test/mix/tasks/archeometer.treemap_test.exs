defmodule Mix.Tasks.Arch.TreemapTest do
  use ExUnit.Case

  @test_db "test/resources/db/archeometer_jissai.db"

  describe "successful cases" do
    test "without parameters" do
      assert :ok = Mix.Task.rerun("arch.treemap", ["--db", @test_db])
    end
  end

  describe "failing invocation" do
    test "wrong params" do
      assert {:error, :wrong_arguments} = Mix.Task.rerun("arch.treemap", ["--foo", "bar"])
    end

    test "non-existing db" do
      assert {:error, "Database is not existent" <> _} =
               Mix.Task.rerun("arch.treemap", ["--db", "foo.db"])
    end

    test "unsupported output format" do
      assert {:error, :unsupported_output_format} =
               Mix.Task.rerun("arch.treemap", ["--db", @test_db, "--format", "foo"])
    end

    test "invalid app name" do
      assert {:error, :no_modules_matched} =
               Mix.Task.rerun("arch.treemap", ["--db", @test_db, "--app", "foo"])
    end

    test "invalid namespace" do
      assert {:error, :no_modules_matched} =
               Mix.Task.rerun("arch.treemap", ["--db", @test_db, "--ns", "foo"])
    end
  end
end
