defmodule Archeometer.ReportTest do
  use ExUnit.Case

  @test_db "test/resources/db/archeometer_jissai.db"
  @base_report_path "reports/test/static"

  describe "successful invocation" do
    setup do
      # This line is a temporal workaround
      File.cp!(@test_db, Archeometer.Repo.default_db_name())

      on_exit(fn ->
        # This line is a temporal workaround
        File.rm!(Archeometer.Repo.default_db_name())

        File.rm_rf("#{@base_report_path}/html")
        File.rm_rf("#{@base_report_path}/livemd")
      end)
    end

    test "HTML report with db parameter" do
      assert :ok = Mix.Task.rerun("arch.report", ["--db", @test_db])

      report_path = "#{@base_report_path}/html/"

      assert File.exists?("#{report_path}/index.html")
      assert File.exists?("#{report_path}/jissai.html")
      assert File.exists?("#{report_path}/jissai_web.html")
    end

    test "Livebook report with db parameter" do
      assert :ok = Mix.Task.rerun("arch.report", ["--db", @test_db, "--format", "livemd"])

      report_path = "#{@base_report_path}/livemd/"

      assert File.exists?("#{report_path}/index.livemd")
      assert File.exists?("#{report_path}/jissai.livemd")
      assert File.exists?("#{report_path}/jissai_web.livemd")
    end
  end

  describe "failed invocation" do
    import ExUnit.CaptureIO

    test "invalid flag" do
      assert capture_io(fn ->
               Mix.Task.rerun("arch.report", ["--foo", "bar"])
             end) =~ ~r/Usage: mix arch.report/
    end

    test "non existent db" do
      assert capture_io(fn ->
               Mix.Task.rerun("arch.report", ["--db", "foobar.db"])
             end) =~ ~r/Usage: mix arch.report/
    end

    test "no supported format" do
      assert capture_io(fn ->
               Mix.Task.rerun("arch.report", ["--db", @test_db, "--format", "foobar"])
             end) =~ ~r/Usage: mix arch.report/
    end
  end
end
