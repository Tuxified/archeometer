defmodule Archeometer.CollectTest do
  use ExUnit.Case, async: false
  import ExUnit.CaptureIO
  @moduletag timeout: :infinity

  setup do
    rm_default_db()

    on_exit(fn ->
      rm_default_db()
    end)
  end

  defp rm_default_db(), do: File.rm(Archeometer.Repo.default_db_name())

  describe "successful invocation" do
    test "no args" do
      assert :ok = Mix.Task.rerun("arch.collect.static")
      assert :ok = Mix.Task.rerun("arch.collect.xrefs")
      assert :ok = Mix.Task.rerun("arch.collect.apps")
      assert :ok = Mix.Task.rerun("arch.collect.ecto")
      assert :ok = Mix.Task.rerun("arch.collect.credo_issues")

      # This works on 'mix test' but no with 'mix coveralls', obviously!
      # assert :ok = Mix.Task.rerun("arch.collect.coverage")
    end

    test "collect without coverage" do
      assert :ok = Mix.Task.rerun("arch.collect", ["--no-coverage"])
    end

    # This test would improve coverage but it's unreliable since triggers dependencies recompilation
    # test "collect without coverage and including external deps" do
    # assert :ok = Mix.Task.rerun("arch.collect", ["--no-coverage", "--include-deps", "temp*"])
    # end
  end

  describe "failed invocation with unknown flag" do
    test "collect" do
      assert capture_io(fn ->
               {:error, :wrong_arguments} =
                 Mix.Task.rerun("arch.collect", ["--foo", "--no-coverage"])
             end) =~ ~r/Usage: mix arch.collect/
    end

    test "collect.static" do
      assert capture_io(fn ->
               {:error, :wrong_arguments} = Mix.Task.rerun("arch.collect.static", ["--foo"])
             end) =~ ~r/Usage: mix arch.collect.static/
    end

    test "collect.xrefs" do
      assert capture_io(fn ->
               {:error, :wrong_arguments} = Mix.Task.rerun("arch.collect.xrefs", ["--foo"])
             end) =~ ~r/Usage: mix arch.collect.xrefs/
    end

    test "collect.apps" do
      assert capture_io(fn ->
               {:error, :wrong_arguments} = Mix.Task.rerun("arch.collect.apps", ["--foo"])
             end) =~ ~r/Usage: mix arch.collect.apps/
    end

    test "collect.ecto" do
      assert capture_io(fn ->
               {:error, :wrong_arguments} = Mix.Task.rerun("arch.collect.ecto", ["--foo"])
             end) =~ ~r/Usage: mix arch.collect.ecto/
    end
  end

  describe "failed invocation without running collect.xrefs first" do
    test "collect.xrefs" do
      assert capture_io(fn ->
               {:error, :no_static_analysis_found} = Mix.Task.rerun("arch.collect.xrefs", [])
             end) =~ ~r/Usage: mix arch.collect.xrefs/
    end

    test "collect.apps" do
      assert capture_io(fn ->
               {:error, :no_static_analysis_found} = Mix.Task.rerun("arch.collect.apps", [])
             end) =~ ~r/Usage: mix arch.collect.apps/
    end

    test "collect.ecto" do
      assert capture_io(fn ->
               {:error, :no_static_analysis_found} = Mix.Task.rerun("arch.collect.ecto", [])
             end) =~ ~r/Usage: mix arch.collect.ecto/
    end

    test "collect.credo_issues" do
      assert capture_io(fn ->
               {:error, :no_static_analysis_found} =
                 Mix.Task.rerun("arch.collect.credo_issues", [])
             end) =~ ~r/Usage: mix arch.collect.credo_issues/
    end
  end
end
