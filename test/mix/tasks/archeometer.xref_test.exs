defmodule Mix.Tasks.Arch.XrefTest do
  use ExUnit.Case

  @test_db "test/resources/db/archeometer_jissai.db"

  describe "successful invocation" do
    # All successful invocations require the --db parameter

    test "no args" do
      assert :ok = Mix.Task.rerun("arch.xref", ["--db", @test_db])
    end

    test "--app filter" do
      assert :ok = Mix.Task.rerun("arch.xref", ["--db", @test_db, "--app", "jissai"])
    end

    test "--origin flag" do
      assert :ok = Mix.Task.rerun("arch.xref", ["--db", @test_db, "--origin", "Jissai.Reports"])
    end

    test "--dest flag" do
      assert :ok = Mix.Task.rerun("arch.xref", ["--db", @test_db, "--dest", "Jissai.Reports"])
    end

    test "--origin and --dest flags" do
      assert :ok =
               Mix.Task.rerun("arch.xref", [
                 "--db",
                 @test_db,
                 "--origin",
                 "--dest",
                 "Jissai.Reports"
               ])
    end
  end

  describe "failing invocation" do
    test "wrong params" do
      assert {:error, :wrong_arguments} = Mix.Task.rerun("arch.xref", ["--foo", "bar"])
    end

    test "non-existing db" do
      assert {:error, "Database is not existent" <> _} =
               Mix.Task.rerun("arch.xref", ["--db", "foo.db"])
    end

    test "unsupported output format" do
      assert {:error, :unsupported_output_format} =
               Mix.Task.rerun("arch.xref", ["--db", @test_db, "--format", "foo"])
    end

    test "invalid app name" do
      assert {:error, :no_modules_matched} =
               Mix.Task.rerun("arch.xref", ["--db", @test_db, "--app", "foo"])
    end

    test "invalid namespace" do
      assert {:error, :no_modules_matched} =
               Mix.Task.rerun("arch.xref", ["--db", @test_db, "--ns", "foo"])
    end

    test "incommpatible parameters" do
      assert {:error, :incompatible_parameters} =
               Mix.Task.rerun("arch.xref", [
                 "--db",
                 @test_db,
                 "--app",
                 "jissai",
                 "JissaiWeb.PageView",
                 "JissaiWeb"
               ])
    end

    test "can't write png to console" do
      assert {:error, :png_not_writable_to_console} =
               Mix.Task.rerun("arch.xref", ["--db", @test_db, "--format", "png"])
    end
  end
end
