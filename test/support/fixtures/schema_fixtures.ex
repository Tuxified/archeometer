defmodule Archeometer.SchemaFixtures do
  @moduledoc """
  This module uses `Archeometer.Schema` to generate some modules. These modules
  are later used as a base in tests that require a schema.
  """

  defmacro __using__(_opts) do
    quote do
      defmodule ModuleA do
        @moduledoc false

        use Archeometer.Schema

        alias unquote(__CALLER__.module).ModuleB

        defschema(:module_a) do
          field(:id, primary_key: true)
          has(ModuleB)
        end
      end

      defmodule ModuleB do
        @moduledoc false

        use Archeometer.Schema

        alias unquote(__CALLER__.module).{ModuleA, ModuleC}

        defschema(:module_b) do
          field(:id, primary_key: true)
          has(ModuleC)
          belongs_to(ModuleA)
        end
      end

      defmodule ModuleC do
        @moduledoc false

        use Archeometer.Schema

        alias unquote(__CALLER__.module).ModuleB

        defschema(:module_c) do
          field(:id, primary_key: true)
          belongs_to(ModuleB)
        end
      end
    end
  end
end
