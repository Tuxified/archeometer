defmodule Archeometer.DumpStatsFixtures do
  @moduledoc """
  Module that generates fixtures to be used on `Archeometer.Util.DumpStats`
  """
  def apps_fixtures() do
    Enum.map(
      1..4,
      &%{app: :"app_#{&1}", is_external: Enum.random([true, false])}
    )
  end

  def modules_fixtures() do
    Enum.map(
      1..10,
      &%{
        name: "App.Mod#{&1}",
        num_lines: Enum.random(30..200),
        path: "lib/app/mod#{&1}.ex",
        is_test: Enum.random([0, 1])
      }
    )
  end

  def defs_fixtures() do
    1..20
    |> Enum.chunk_every(2)
    |> Enum.with_index()
    |> Enum.flat_map(fn {defs, mod_id} ->
      Enum.map(
        defs,
        &%{
          name: :"def_#{&1}",
          module: "App.Mod#{mod_id + 1}",
          cc: Enum.random(1..3),
          num_lines: Enum.random(1..20),
          num_args: Enum.random(0..4),
          args: "",
          type: Enum.random([:def, :defp])
        }
      )
    end)
  end

  def coverage_fixtures() do
    1..10
    |> Enum.map(
      &%{
        module: :"Elixir.App.Mod#{&1}",
        coverage: Enum.random(0..10)
      }
    )
  end

  def app_id_fixtures() do
    1..10
    |> Enum.chunk_every(3)
    |> Enum.with_index()
    |> Enum.map(fn {modules, app_id} ->
      %{
        app: :"app_#{app_id + 1}",
        modules: Enum.map(modules, &:"Elixir.App.Mod#{&1}")
      }
    end)
  end

  def xrefs_fixtures() do
    1..10
    |> Enum.map(&:"Elixir.App.Mod#{&1}")
    |> Enum.shuffle()
    |> Enum.chunk_every(2)
    |> Enum.map(fn [caller, callee] ->
      %{
        caller: caller,
        callee: callee,
        type: Enum.random(["require", "struct", "remote_function"]),
        line: Enum.random(1..40)
      }
    end)
  end

  def structs_fixtures() do
    1..10
    |> Enum.shuffle()
    |> Enum.take(3)
    |> Enum.map(&:"Elixir.App.Mod#{&1}")
  end

  def behaviours_fixtures() do
    modules_fixtures()
    |> Enum.shuffle()
    |> Enum.take(3)
    |> Enum.map(fn module ->
      %{
        name: Enum.random(["Plug", "GenServer", "Supervisor", "Application"]),
        module: module.name
      }
    end)
  end

  # some randomly selected checks
  @credo_checks [
    "Credo.Check.Readability.FunctionNames",
    "Credo.Check.Readability.VariableNames",
    "Credo.Check.Refactor.CaseTrivialMatches",
    "Credo.Check.Refactor.MapMap",
    "Credo.Check.Refactor.WithClauses",
    "Credo.Check.Consistency.LineEndings",
    "Credo.Check.Design.TagTODO",
    "Credo.Check.Warning.IoInspect",
    "Credo.Check.Warning.UnusedOperation"
  ]

  def credo_issues_fixtures() do
    modules_fixtures()
    |> Enum.shuffle()
    |> Enum.take(Enum.random(1..10))
    |> Enum.map(fn module ->
      %Archeometer.Collect.CredoIssues{
        module: module.name,
        severity: Enum.random(1..5),
        line_no: Enum.random(1..1000),
        check: Enum.random(@credo_checks)
      }
    end)
  end
end
