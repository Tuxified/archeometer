# Overview

Archeometer is a tool to collect as much information about your project as possible, and make it available for you to discover the hidden structure and patterns of your projects. Information like:
+ Number of modules, functions and macros per application
+ Local dependencies by type: function calls, macro calls, structure calls
+ Test coverage per module and function
+ Function and macro visibility, length in code lines, cyclomatic complexity

Among other information. This is made available through a generated report in HTML and through an Elixir API for you to build your own diagnostics.

The goal is to give you an insight into how a project is organized and how it could be improved. 

To get started visit
+ [Installation](./installation.md)
+ [Basic Usage](./basic_usage.md)
