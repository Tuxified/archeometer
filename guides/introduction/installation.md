# Installation

This is a Mix package, so you must install it [using Mix](http://elixir-lang.org/getting-started/mix-otp/introduction-to-mix.html) (Available in [Hex](https://hex.pm/packages/archeometer)). To use it you can add it in your project `mix.exs`:
```elixir
def deps do
  [
    {:archeometer, "~> 0.2", only: [:dev, :test], runtime: false},
  ]
end
```

Or alternatively, you can clone [this repository](https://gitlab.com/bunsan1/archeometer/) and add the dependency as a  `path: "path/to/the/repo"` instead.

And then just run
```sh
mix deps.get
```
