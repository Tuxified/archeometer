defmodule Archeometer.MixProject do
  use Mix.Project

  def project do
    [
      app: :archeometer,
      version: "0.4.2",
      elixir: ">= 1.10.0",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test,
        "arch.collect.coverage": :test
      ],

      # documentation information
      name: "Archeometer",
      description:
        "Toolkit to explore the architecture, design and quality of your elixir projects",
      package: [
        name: "archeometer",
        licenses: ["MIT"],
        links: %{"Gitlab" => "https://gitlab.com/MachinesAreUs/archeometer"}
      ],
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :tools],
      env: [test_db: "archeometer_test.db"]
    ]
  end

  defp docs() do
    [
      main: "overview",
      extras: extras(),
      extra_section: "GUIDES",
      groups_for_extras: groups_for_extras(),
      groups_for_modules: groups_for_modules(),
      nest_modules_by_prefix: nest_modules_by_prefix()
    ]
  end

  defp extras() do
    [
      "guides/introduction/overview.md",
      "guides/introduction/installation.md",
      "guides/introduction/basic_usage.md",
      "guides/cql/queries_and_schemas.md",
      "cheatsheets/tasks.cheatmd",
      "cheatsheets/cql.cheatmd"
    ]
  end

  defp groups_for_extras() do
    [
      Introduction: Path.wildcard("guides/introduction/*.md"),
      "Code Query Language": Path.wildcard("guides/cql/*.md")
    ]
  end

  defp groups_for_modules() do
    [
      "Project Data Collection": ~r{^Archeometer.Collect},
      "Report generation": ~r{^Archeometer.Reports},
      Analysis: ~r{^Archeometer.Analysis},
      Visualization: ~r{^Archeometer.Graphs},
      "Query Language": ~r{^Archeometer.Query},
      "Project Schema": ~r{^Archeometer.Schema},
      "Executing Queries": ~r{^Archeometer.Repo},
      Utilities: ~r{^Archeometer.Util}
    ]
  end

  # Modules with names too long to show in the side bar
  defp nest_modules_by_prefix() do
    [
      Archeometer.Analysis.DSM,
      Archeometer.Graphs,
      Archeometer.Query.Term,
      Archeometer.Reports,
      Archeometer.Schema,
      Archeometer.Collect
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:temp, "~> 0.4"},
      {:credo, "~> 1.6.7"},
      {:exqlite, "~> 0.13"},
      {:elixir_uuid, "~> 1.2"},
      {:slime, "~> 1.3"},
      {:css_colors, "~> 0.2.2"},
      {:wild, "~> 1.0"},
      {:memoize, "~> 1.4"},
      # Dev only deps
      {:ex_doc, "~> 0.29", only: :dev, runtime: false},
      # Test only deps
      {:excoveralls, "~> 0.15", only: :test},
      {:magic_number, "~> 0.0.4", only: :test},
      {:ecto, "~> 3.0", only: :test}
    ]
  end
end
