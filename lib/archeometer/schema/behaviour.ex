defmodule Archeometer.Schema.Behaviour do
  @moduledoc """
  Represents an implementation of a Behaviour into a module.

  This schema has te following fields:
  - `name` is the name of the implemented behaviour.
  - `module` is the reference to `Archeometer.Schema.Module`.
    It is the module that implements the behaviour.
  """

  use Archeometer.Schema

  alias Archeometer.Schema.Module

  defschema(:behaviours) do
    field(:id, primary_key: true)
    field(:name)
    belongs_to(Module)
  end
end
