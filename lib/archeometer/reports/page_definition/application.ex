defmodule Archeometer.Reports.PageDefinition.Application do
  @moduledoc """
  Defines the structure of the page definition for a single application.
  """

  alias Archeometer.Reports.Page
  alias Archeometer.Reports.Section
  alias Archeometer.Reports.FragmentDefs

  def definition(app_name) do
    %Page.Definition{
      id: app_name,
      sections: [
        %Section.Definition{
          id: :size,
          desc: "Size",
          fragments: [
            FragmentDefs.Size.largest_modules(),
            FragmentDefs.Size.module_size_treemap()
          ]
        },
        %Section.Definition{
          id: :complexity,
          desc: "Complexity",
          fragments: [
            FragmentDefs.Complexity.most_complex_modules(),
            FragmentDefs.Complexity.most_complex_functions()
          ]
        },
        %Section.Definition{
          id: :core,
          desc: "Core Modules",
          fragments: [
            FragmentDefs.Core.modules_with_most_deps(),
            FragmentDefs.Core.core_modules(),
            FragmentDefs.Core.core_modules_graph()
          ]
        },
        %Section.Definition{
          id: :building_blocks,
          desc: "Building Blocks",
          fragments: [
            FragmentDefs.BuildingBlocks.most_used_modules()
          ]
        },
        %Section.Definition{
          id: :api,
          desc: "Public API",
          fragments: [
            FragmentDefs.API.modules_with_most_public_funs()
          ]
        },
        %Section.Definition{
          id: :ecto,
          desc: "Ecto",
          fragments: [
            FragmentDefs.Ecto.ecto_schemas(),
            FragmentDefs.Ecto.ecto_dependency_graph()
          ]
        },
        %Section.Definition{
          id: :otp,
          desc: "OTP",
          fragments: [
            FragmentDefs.OTP.otp_behaviours()
          ]
        },
        %Section.Definition{
          id: :dsm,
          desc: "Dependency Cycles",
          fragments: [
            FragmentDefs.DepsAnalysis.dsm_matrix(),
            FragmentDefs.DepsAnalysis.dsm_largest_deps_group(),
            FragmentDefs.DepsAnalysis.dsm_largest_deps_group_graph()
          ]
        },
        %Section.Definition{
          id: :test_coverage,
          desc: "Test Coverage",
          fragments: [
            FragmentDefs.TestCoverage.modules_with_least_coverage()
          ]
        }
      ]
    }
  end
end
