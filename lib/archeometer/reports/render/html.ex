defmodule Archeometer.Reports.Render.Html do
  @moduledoc """
  Renders a page into HTML.
  """
  require Slime

  Slime.function_from_file(
    :def,
    :render,
    "priv/templates/report/html/base_report.slime",
    [
      :app_data,
      :page_names,
      :renderer
    ]
  )

  Slime.function_from_file(:def, :main_report, "priv/templates/report/html/main_report.slime", [
    :app_data,
    :renderer
  ])

  Slime.function_from_file(
    :def,
    :nav_side_menu,
    "priv/templates/report/html/nav_side_menu.slime",
    [
      :page_names,
      :renderer
    ]
  )

  Slime.function_from_file(:def, :header, "priv/templates/report/html/header.slime", [
    :app_data,
    :renderer
  ])

  Slime.function_from_file(:def, :sections, "priv/templates/report/html/sections.slime", [
    :sections,
    :_renderer
  ])

  Slime.function_from_file(:def, :fragment, "priv/templates/report/html/fragment.slime", [
    :fragment,
    :renderer
  ])

  Slime.function_from_file(
    :def,
    :code_fragment,
    "priv/templates/report/html/code_fragment.slime",
    [
      :fragment,
      :renderer
    ]
  )

  Slime.function_from_file(:def, :button_tab, "priv/templates/report/html/button_tab.slime", [
    :fragment,
    :type,
    :_renderer
  ])

  Slime.function_from_file(:def, :tab_content, "priv/templates/report/html/tab_content.slime", [
    :code_lang,
    :uuid,
    :code_fragment,
    :renderer
  ])

  Slime.function_from_file(:def, :button_copy, "priv/templates/report/html/button_copy.slime", [
    :uuid,
    :code_lang,
    :_renderer
  ])

  Slime.function_from_file(:def, :result_table, "priv/templates/report/html/result_table.slime", [
    :fragment,
    :_renderer
  ])

  Slime.function_from_file(
    :def,
    :fragment_result,
    "priv/templates/report/html/fragment_result.slime",
    [
      :fragment,
      :renderer
    ]
  )

  Slime.function_from_file(
    :def,
    :svg_container,
    "priv/templates/report/html/svg_container.slime",
    [
      :fragment_uuid,
      :svg_path,
      :_renderer
    ]
  )

  Slime.function_from_file(
    :def,
    :sidemenu_item,
    "priv/templates/report/html/sidemenu_item.slime",
    [
      :item,
      :_renderer
    ]
  )
end
