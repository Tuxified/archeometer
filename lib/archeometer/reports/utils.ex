defmodule Archeometer.Reports.Utils do
  @moduledoc """
  Utility functions for generating reports
  """

  alias Archeometer.Reports.Config

  def prepare_paths(:html) do
    report_path = report_path(:html)

    [
      Path.expand("img/", report_path),
      Path.expand("js/", report_path),
      Path.expand("css/", report_path),
      Path.expand("assets/", report_path)
    ]
    |> Enum.each(fn path -> File.mkdir_p!(path) end)

    report_path(:img) |> File.mkdir_p!()
  end

  def prepare_paths(:livemd) do
    report_path(:livemd) |> File.mkdir_p!()
  end

  def copy_assets() do
    template_base_path = Archeometer.Util.PathHelper.template_path("report/html")
    report_path = report_path(:html)

    ["css", "js", "assets"]
    |> Enum.each(fn subdir ->
      src_path = Path.expand(subdir, template_base_path)
      dest_path = Path.expand(subdir, report_path)
      File.cp_r!(src_path, dest_path)
    end)
  end

  def copy_images() do
    src_path = report_path(:img)
    report_path = report_path(:html)
    dest_path = Path.expand("img", report_path)
    File.cp_r!(src_path, dest_path)
  end

  def report_path(:html), do: Config.report_path(:html)

  def report_path(:livemd), do: Config.report_path(:livemd)

  def report_path(:img), do: Config.report_path(:img)

  def write_to_file(str, type, fname) when type in [:html, :livemd] do
    fname
    |> Path.expand(report_path(type))
    |> File.write!(str)
  end
end
