defmodule Archeometer.Reports.Config do
  @moduledoc """
  Provides functions to access the configuration of reports.
  """

  def report_path(:html) do
    Path.expand("html", static_report_path())
  end

  def report_path(:img) do
    Path.expand("img", static_report_path())
  end

  def report_path(:livemd) do
    Path.expand("livemd", static_report_path())
  end

  def static_report_path() do
    "./reports/#{Mix.env()}/static/"
  end

  def static_report_img_path() do
    Path.expand("img/", static_report_path())
  end
end
