defmodule Archeometer.Analysis.Xref.Centrality do
  @moduledoc false

  @doc """
  Calculates the centrality of everynode in a graph represented by an adjacency list.

  ## Examples

  No centrality at all

    iex> xrefs = %{
    ...>   "AnalyticsBuilder" => [],
    ...>   "AtamaData" => [],
    ...>   "AttendanceProcess" => [],
    ...>   "ClassDynamicsProcess" => [],
    ...>   "DataAnalysis" => [],
    ...>   "DataAnalysisHelper" => [],
    ...>   "ParticipationProcesss" => [],
    ...>   "UserRoleHelper" => []
    ...> }
    iex> degree_centrality(xrefs)
    %{
      "AnalyticsBuilder" => 0.0,
      "AtamaData" => 0.0,
      "AttendanceProcess" => 0.0,
      "ClassDynamicsProcess" => 0.0,
      "DataAnalysis" => 0.0,
      "DataAnalysisHelper" => 0.0,
      "ParticipationProcesss" => 0.0,
      "UserRoleHelper" => 0.0
    }

  A complex graph

    iex> xrefs = %{
    ...>   "AnalyticsBuilder" =>
    ...>     [
    ...>       "AttendanceProcess",
    ...>       "ParticipationProcesss",
    ...>       "ClassDynamicsProcess",
    ...>       "UserRoleHelper"
    ...>     ],
    ...>   "AtamaData" => ["UserRoleHelper", "DataAnalysisHelper"],
    ...>   "AttendanceProcess" => ["AtamaData", "DataAnalysis"],
    ...>   "ClassDynamicsProcess" => ["DataAnalysis", "AtamaData", "DataAnalysisHelper"],
    ...>   "DataAnalysis" => ["AtamaData"],
    ...>   "DataAnalysisHelper" => ["DataAnalysis"],
    ...>   "ParticipationProcesss" => ["AtamaData", "DataAnalysis"],
    ...>   "UserRoleHelper" => ["AnalyticsBuilder", "AtamaData", "DataAnalysis"]
    ...> }
    iex> degree_centrality(xrefs)
    %{
      "AnalyticsBuilder" => 0.7142857142857143,
      "AtamaData" => 1.0,
      "AttendanceProcess" => 0.42857142857142855,
      "ClassDynamicsProcess" => 0.5714285714285714,
      "DataAnalysis" => 0.8571428571428571,
      "DataAnalysisHelper" => 0.42857142857142855,
      "ParticipationProcesss" => 0.42857142857142855,
      "UserRoleHelper" => 0.7142857142857143
    }
  """
  def degree_centrality(xrefs) do
    outgoing = Enum.map(xrefs, fn {k, v} -> {k, length(v)} end) |> Map.new()
    incoming = Map.values(xrefs) |> List.flatten() |> Enum.frequencies()
    degrees = Map.merge(outgoing, incoming, fn _k, v1, v2 -> v1 + v2 end)

    max_degree =
      case Map.values(degrees) do
        [] -> 0
        values -> Enum.max(values)
      end

    Enum.reduce(degrees, %{}, fn {k, v}, acc ->
      degree =
        case max_degree do
          0 -> 0.0
          _ -> v / max_degree
        end

      Map.put(acc, k, degree)
    end)
  end
end
