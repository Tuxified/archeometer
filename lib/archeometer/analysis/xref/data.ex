defmodule Archeometer.Analysis.Xref.Data do
  import Archeometer.Query
  alias Archeometer.Repo
  alias Archeometer.Schema.XRef
  alias Archeometer.Schema.Module

  @moduledoc false

  def get_modules(:none, "*", db_name) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            name: m.name
          ]
        ),
        [],
        db_name
      )

    modules
  end

  def get_modules(:none, ns, db_name) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [name: m.name],
          where: m.name == ^ns or like(m.name, ^(ns <> ".%"))
        ),
        [],
        db_name
      )

    modules
  end

  def get_modules(app, "*", db_name) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            name: m.name
          ],
          where: m.app.name == ^app
        ),
        [],
        db_name
      )

    modules
  end

  def get_modules(app, ns, db_name) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            name: m.name
          ],
          where: m.app.name == ^app and (m.name == ^ns or like(m.name, ^(ns <> ".%")))
        ),
        [],
        db_name
      )

    modules
  end

  def neighbors(module, :outgoing, db_name) do
    Repo.all(
      from(x in XRef,
        select: [
          name: x.callee.name
        ],
        where: x.caller.name == ^module
      ),
      [],
      db_name
    )
  end

  def neighbors(module, :incoming, db_name) do
    Repo.all(
      from(x in XRef,
        select: [
          name: x.caller.name
        ],
        where: x.callee.name == ^module
      ),
      [],
      db_name
    )
  end

  def neighbors(module, :both, db_name) do
    incoming = neighbors(module, :incoming, db_name)
    outgoing = neighbors(module, :outgoing, db_name)
    Map.update(incoming, :rows, [], &(&1 ++ outgoing.rows))
  end

  def module_exists?(module, db_name) do
    %{rows: result} =
      Repo.all(
        from(m in Module,
          select: [m.name],
          where: m.name == ^module
        ),
        [],
        db_name
      )

    length(result) > 0
  end
end
