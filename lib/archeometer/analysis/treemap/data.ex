defmodule Archeometer.Analysis.Treemap.Data do
  @moduledoc false

  import Archeometer.Query
  alias Archeometer.Repo
  alias Archeometer.Schema.Module

  @supported_metrics [:size, :functions, :issues]

  def get_data(metric, app, namespace, db_name, skip_tests) when metric in @supported_metrics do
    get_modules_and_metric(metric, app, namespace, db_name, skip_tests)
  end

  def get_data(_metric, _app, _namespace, _db, _skip_tests) do
    {:error, :unsupported_metric}
  end

  defp get_modules_and_metric(metric, app, ns, db_name, skip_tests) do
    is_tests_vals = if skip_tests, do: [false], else: [true, false]
    modules = get_modules(metric, app, ns, db_name, is_tests_vals)

    Enum.map(modules, fn [id, name, num_lines] -> {id, name, num_lines} end)
  end

  defp get_modules(:size, :none, "*", db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_lines: m.num_lines
          ],
          where: m.is_test in ^is_tests_vals
        ),
        [],
        db_name
      )

    modules
  end

  defp get_modules(:size, :none, ns, db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_lines: m.num_lines
          ],
          where:
            (m.name == ^ns or like(m.name, ^(ns <> ".%"))) and
              m.is_test in ^is_tests_vals
        ),
        [],
        db_name
      )

    modules
  end

  defp get_modules(:size, app, "*", db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_lines: m.num_lines
          ],
          where:
            m.app.name == ^app and
              m.is_test in ^is_tests_vals
        ),
        [],
        db_name
      )

    modules
  end

  defp get_modules(:size, app, ns, db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_lines: m.num_lines
          ],
          where:
            m.app.name == ^app and
              (m.name == ^ns or like(m.name, ^(ns <> ".%"))) and
              m.is_test in ^is_tests_vals
        ),
        [],
        db_name
      )

    modules
  end

  defp get_modules(:functions, :none, "*", db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_functions: count(m.functions.id)
          ],
          where: m.is_test in ^is_tests_vals,
          group_by: m.name
        ),
        [],
        db_name
      )

    modules
  end

  defp get_modules(:functions, :none, ns, db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_functions: count(m.functions.id)
          ],
          where:
            (m.name == ^ns or like(m.name, ^(ns <> ".%"))) and
              m.is_test in ^is_tests_vals,
          group_by: m.name
        ),
        [],
        db_name
      )

    modules
  end

  defp get_modules(:functions, app, "*", db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_functions: count(m.functions.id)
          ],
          where:
            m.app.name == ^app and
              m.is_test in ^is_tests_vals,
          group_by: m.name
        ),
        [],
        db_name
      )

    modules
  end

  defp get_modules(:functions, app, ns, db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_functions: count(m.functions.id)
          ],
          where:
            m.app.name == ^app and
              (m.name == ^ns or like(m.name, ^(ns <> ".%"))) and
              m.is_test in ^is_tests_vals,
          group_by: m.name
        ),
        [],
        db_name
      )

    modules
  end

  defp get_modules(:issues, :none, "*", db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_issues: count(m.credo_issues.id)
          ],
          where: m.is_test in ^is_tests_vals,
          group_by: m.name
        ),
        [],
        db_name
      )

    modules
  end

  defp get_modules(:issues, :none, ns, db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_issues: count(m.credo_issues.id)
          ],
          where:
            (m.name == ^ns or like(m.name, ^(ns <> ".%"))) and
              m.is_test in ^is_tests_vals,
          group_by: m.id
        ),
        [],
        db_name
      )

    modules
  end

  defp get_modules(:issues, app, "*", db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_issues: count(m.credo_issues.id)
          ],
          where:
            m.app.name == ^app and
              m.is_test in ^is_tests_vals,
          group_by: m.name
        ),
        [],
        db_name
      )

    modules
  end

  defp get_modules(:issues, app, ns, db_name, is_tests_vals) do
    %{rows: modules} =
      Repo.all(
        from(m in Module,
          select: [
            id: m.id,
            name: m.name,
            num_issues: count(m.credo_issues.id)
          ],
          where:
            m.app.name == ^app and
              (m.name == ^ns or like(m.name, ^(ns <> ".%"))) and
              m.is_test in ^is_tests_vals,
          group_by: m.name
        ),
        [],
        db_name
      )

    modules
  end
end
