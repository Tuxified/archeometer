defmodule Archeometer do
  @moduledoc """
  # Archeometer

  Archeometer is a code analysis tool to help diagnose potential problems and
  refactoring points, particularly in large code bases.

  It does this by collecting as much information about the code as possible and
  storing it in a SQLite database for later processing.

  For now, some parts are implemented as a Credo Plugin, though this will change in the
  future.

  ## Metrics

  For now there are three types of metric:

  - Static: Information about the code AST
  - Compilation: Project/application classification
  - Testing: Code coverage
  - Cross references: Internal dependencies between modules 
  """
end
