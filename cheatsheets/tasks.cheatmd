# Tasks Cheatsheet

## Data Collection
{: .col-2}

### Basic execution

Run all [data collection stages](guides/introduction/basic_usage.md#data-collection-stages)

```console
$ mix arch.collect
```

### Include dependencies

Run all [data collection stages](guides/introduction/basic_usage.md#data-collection-stages) on project and dependencies that match [filter(s)](guides/introduction/basic_usage.md#include-dependencies-in-the-analysis)

```console
$ mix arch.collect --include-deps "phoenix*"
```

### No coverage

Skip [coverage information](Mix.Tasks.Arch.Collect.Coverage.html) gathering stage

```console
$ mix arch.collect --no-coverage
```

## Report
{: .col-2}

To create a report, it's necessary you already successfully ran data collection.

### Basic execution

Generate default HTML static report

```console
$ mix arch.report
```

### Format

Generate [Livebook](https://livebook.dev/) report

```console
$ mix arch.report --format livemd
```

### Limit Query Results

Limit the number of results of the queries used in the report (e.g. to 10 elements)

```console
$ mix arch.report --limit 10
```

## Analysis Common Options

| Option       | Default                       | Supported by                  |
|--------------|-------------------------------|-------------------------------|
| --format     | Depends on task               | apps.xref, dsm, treemap, xref |
| --out        | console                       | apps.xref, dsm, treemap, xref |
| --app        | * (All applications)          | dsm, treemap, xref            |
| --ns         | * (All modules)               | dsm, treemap, xref            |
| --db         | archeometer_<project name>.db | dsm, treemap, xref            |
| --skip-tests | true                          | dsm, treemap                  |

A more complex example using this options. This will generate a DSM from a database that is not the defaultone and consider only modules that belong to *foo* application in the namespace *Foo.Bar* and **not** ignoring the test modules.

```console
$ mix arch.dsm \
  --out foo.bar.svg \
  --format svg \
  --app foo \
  --ns Foo.Bar \
  --skip-tests false \
  --db archeometer_wareware.db
```
## Analysis
{: .col-2}

### Apps XRef

#### Basic execution

Print the dependency graph of applications in console (as text)

```console
$ mix arch.apps.xref
```

#### Format

Following formats are supported

- dot (default)
- png
- svg


#### Include dependencies

Target project and include dependencies that match the [filter(s)](guides/introduction/basic_usage.md#include-dependencies-in-the-analysis)

This option can be used on non-Umbrella projects

```console
$ mix arch.apps.xref --include-deps "phoenix*"
```

### XRef

#### Basic execution

Generate a dependency graph between given modules and print it to console

```console
$ mix arch.xref mod1 mod2 .. modN
```

#### Format

Following formats are supported

- dot (default)
- png
- svg

### Design Structure Matrix (DSM)

#### Basic Execution

Generate a [DSM](https://en.wikipedia.org/wiki/Design_structure_matrix) of all non-test modules and print it to console

```console
$ mix arch.dsm
```

#### Format

Following formats are supported

- txt
- svg

### Treemap

#### Basic Execution

Generate a [Treemap](https://en.wikipedia.org/wiki/Treemapping) of all non-test modules from the project and print it to console

```console
$ mix arch.treemap
```

#### Metric

For now only size metric is supported

```console
$ mix arch.treemap --metric size
```

## Collect Subtasks
{: .col-2}

### Include Dependencies

Just as in the main Collect task you can include dependencies (not supported by **arch.collect.coverage**)

```console
$ mix arch.collect.static --include deps "phoenix*"
```

### Collect Static

Target project `ex` files but not dependencies

```console
$ mix arch.collect.static
```

### Collect XRefs

Save information of dependencies between project modules

```console
$ mix arch.collect.xrefs
```

### Collect Apps

Save applications names, modules implemented behaviours and structs from project

```console
$ mix arch.collect.apps
```

### Collect Ecto

Save information of Ecto schema modules and their relationships

```console
$ mix arch.collect.ecto
```

### Collect Coverage

Run project tests files and save coverage information

```console
$ mix arch.collect.coverage
```
